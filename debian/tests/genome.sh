#!/bin/bash
set -x
set -e
#
# Generate a random flame, showing seed numbers used
env seed=$RANDOM issac_seed=$RANDOM flam3-genome > random.flam3
#
# Check flame and render large high quality image
xmllint -noout random.flam3
env ss=12 qs=6 flam3-render < random.flam3
#
# Check rendered image
pngcheck 00000.png
