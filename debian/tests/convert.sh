#!/bin/sh
#
# Test conversion of a Gimp flame
set -e
set -x
#
flam3-convert < debian/test.gimp > convert.flam3
xmllint -noout convert.flam3
flam3-render < convert.flam3
pngcheck 00000.png
