#!/bin/sh
#
# Animation of test.flam3
#
set -e
set -x
#
env frame=10 flam3-animate < /usr/share/flam3/test.flam3
pngcheck 00010.png
