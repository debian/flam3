#!/bin/sh
#
# Rendering of bundled test.flam3
#
set -e
set -x
#
env ss=8 qs=5 flam3-render < /usr/share/flam3/test.flam3
pngcheck 00000.png
pngcheck 00001.png


